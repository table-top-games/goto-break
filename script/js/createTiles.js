function createTiles(
	type, n, text = "", v = "") {
	for (let i = 0; i < n; ++i) {
		document.write(`<div class="card tile ${type}"><span>${text}</span><span class="number">${v}</span></div>`);
	}
}