#include <iostream>

struct Location {
	int x, y, z;
};

class Entity {
	Location location;
};

enum color {
	red,
	green,
	blue,
};
class RobotOwner {
};

RobotOwner* player_red;


class Robot : virtual public Entity {
	RobotOwner* owner;
public:
	Robot() = default;
	explicit Robot(RobotOwner* owner);
	
	virtual void method0() {}
	virtual void method1() {}
	virtual void method2() {}
	virtual void method3() {}
};

Robot::Robot(RobotOwner* owner)
:
	owner{owner}
{}

class WalkingRobot : virtual public Robot {
	
};

class MiningRobot : virtual public Robot {
	
};

class ShootingRobot : virtual public Robot {
	
};



class RobotRed0 : public WalkingRobot, public MiningRobot {
public:
	RobotRed0() = default;
	RobotRed0(RobotOwner* owner);
	
	virtual void method0() override;
};

RobotRed0::RobotRed0(RobotOwner* owner) : Robot{owner}
{
	std::cout << "g";
}

void RobotRed0::method0()
{std::cout << "0";
	//...
}




class RobotRed1 : public WalkingRobot, public MiningRobot {
public:
	RobotRed1() = default;
	RobotRed1(RobotOwner* owner);
	
	virtual void method0() override;
};

RobotRed1::RobotRed1(RobotOwner* owner) : Robot{owner}
{
	std::cout << "g";
}

void RobotRed1::method0()
{std::cout << "1";
	//...
}




class RobotRed2 : public RobotRed0, public RobotRed1 {
public:
	RobotRed2(RobotOwner* owner);
	
	virtual void method0() override;
};

RobotRed2::RobotRed2(RobotOwner* owner) : Robot{owner}
{
	std::cout << "g";
}

void RobotRed2::method0()
{std::cout << "2";
	//...
}




int main(int argc, char *argv[])
{
	Robot&& r = RobotRed2{player_red};
	r.method0();
}